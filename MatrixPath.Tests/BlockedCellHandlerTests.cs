﻿using System;
using System.Collections.Generic;
using MatrixPath.Entities;
using MatrixPath.Implementations;
using MatrixPath.Interfaces;
using Moq;
using NUnit.Framework;
using Unity;

namespace MatrixPath.Tests
{
    [TestFixture]
    public class BlockedCellHandlerTests
    {
        [Test]
        public void Class_Initialization_Fails_When_UnityContainer_Is_Null()
        {
            IUnityContainer unityContainer = null;
            Assert.Throws<ArgumentNullException>(() => new BlockedCellHandler(unityContainer));

        }

        [Test]
        public void Class_Initialization_Fails_When_Dependencies_Are_Not_Added_To_Unity()
        {
            IUnityContainer unityContainer = new UnityContainer();
            Assert.Throws<Unity.ResolutionFailedException>(() => new BlockedCellHandler(unityContainer));

            // When only console Writer is registered
            unityContainer = new UnityContainer();
            var mock1 = new Mock<IBlockedCellInputAcceptanceHandler>();
            unityContainer.RegisterInstance(mock1.Object);
            Assert.Throws<Unity.ResolutionFailedException>(() => new BlockedCellHandler(unityContainer));

            // When only console reader is registered
            unityContainer = new UnityContainer();
           var mock2 = new Mock<IValidateAndAcceptIndividualBlockedCells>();
            unityContainer.RegisterInstance(mock2.Object);
            Assert.Throws<Unity.ResolutionFailedException>(() => new BlockedCellHandler(unityContainer));
        }

        [Test]
        public void Class_Initialization_Succeeds_When_All_Dependencies_Are_Initialized_Properly()
        {
            IUnityContainer unityContainer = new UnityContainer();

            var mockCellInputAcceptanceHandler = new Mock<IBlockedCellInputAcceptanceHandler>();
            unityContainer.RegisterInstance(mockCellInputAcceptanceHandler.Object);

            var mockIndividualCellAcceptanceHandler = new Mock<IValidateAndAcceptIndividualBlockedCells>();
            unityContainer.RegisterInstance(mockIndividualCellAcceptanceHandler.Object);

            var instance = new BlockedCellHandler(unityContainer);

            Assert.IsNotNull(instance);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(99)]
        public void When_A_Grid_Is_Passed_IBlockedCellInputAcceptanceHandler_Is_Called_Excatly_Once(int numberOfCellsToReturn)
        {
            IUnityContainer unityContainer = new UnityContainer();

            var mockCellInputAcceptanceHandler = new Mock<IBlockedCellInputAcceptanceHandler>();
            mockCellInputAcceptanceHandler.Setup(a => a.AcceptAndValidateNumberOfCellsToBlock(It.IsAny<int[,]>()))
                .Returns(numberOfCellsToReturn);

            unityContainer.RegisterInstance(mockCellInputAcceptanceHandler.Object);

            var mockIndividualCellHandler = new Mock<IValidateAndAcceptIndividualBlockedCells>();
            mockIndividualCellHandler
                .Setup(a => a.ValidateAndAcceptBlockedCells(It.IsAny<int[,]>(), numberOfCellsToReturn))
                .Returns(new HashSet<Position>());

            unityContainer.RegisterInstance(mockIndividualCellHandler.Object);

            var instance = new BlockedCellHandler(unityContainer);
            instance.Handle(new int[3, 3]);

            mockCellInputAcceptanceHandler.Verify(a=>a.AcceptAndValidateNumberOfCellsToBlock(It.IsAny<int[,]>()),Times.Once);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(99)]
        public void When_A_Grid_Is_Passed_IValidateAndAcceptIndividualBlockedCells_Is_Called_Exactly_Once(int numberOfCellsToReturn)
        {
            IUnityContainer unityContainer = new UnityContainer();

            var mockCellInputAcceptanceHandler = new Mock<IBlockedCellInputAcceptanceHandler>();
            mockCellInputAcceptanceHandler.Setup(a => a.AcceptAndValidateNumberOfCellsToBlock(It.IsAny<int[,]>()))
                .Returns(numberOfCellsToReturn);

            unityContainer.RegisterInstance(mockCellInputAcceptanceHandler.Object);

            var hs = new HashSet<Position>();
            for (int i = 0; i < numberOfCellsToReturn; i++)
            {
                hs.Add(new Position(i, i));
            }

            var mockIndividualCellHandler = new Mock<IValidateAndAcceptIndividualBlockedCells>();
            mockIndividualCellHandler
                .Setup(a => a.ValidateAndAcceptBlockedCells(It.IsAny<int[,]>(), numberOfCellsToReturn))
                .Returns(hs);

            unityContainer.RegisterInstance(mockIndividualCellHandler.Object);

            var instance = new BlockedCellHandler(unityContainer);
           var returnHs = instance.Handle(new int[3, 3]);


           mockIndividualCellHandler.Verify(a => a.ValidateAndAcceptBlockedCells(It.IsAny<int[,]>(),numberOfCellsToReturn), Times.Once);
           Assert.AreEqual(returnHs,hs);
        }
    }
}
