# Matrix Path

Given an M x M matrix, the task is to find if there exists any path from the first cell to the last cell in the grid.

Assumptions

  - Any cell except the first and the last cell can be blocked.
  - Diagonal Traversal is not possible.

### Approach

The solution is implemented in compliance with the SOLID principles. It is also highly unit testable. Each task involved in the solution is decoupled and extracted into a stand alone unit testable component. All dependencies are resolved using Unity

### Tech

  - .net 4.7.2 C#,
  - Unity,
  - Moq,
  - NUnit

### Starting The App
Open the MatrixPath Solution and ensure the project 'MatrixPath' is set as the starting project. Now run the project. 

### Note

 - 'Diagonal hopping' is implemented but not added to the list of action.
 - Order of registration matters. I.e if the hops are registered as (Right, Down), then it will try to traverse Right first and then Down (if not successful)

### Limitations
 - Dependency injection mapping is not external (i.e not inside any config or json file). To change the mapping or add new dependencies, *DependencyContainer*  class has to be modified (breaking the OCP). The entire dependency mapping can be removed from a class file to a config/json and on app load, this can be auto mapped using Reflection. But this is beyond the scope of the current solution.
 - Due to severe time constraints, a complete unit testing is not done. Only a single unit test 'BlockedCellHandlerTests' is implemented to demonstrate unit testing capabilities.