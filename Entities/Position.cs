﻿namespace MatrixPath.Entities
{
    public struct Position
    {
        public Position(int i, int j)
        {
            I = i;
            J = j;
        }

        public int I { get; }

        public int J { get; }

        public static bool operator ==(Position a, Position b)
        {
            if (Equals(a, b)) return true;

            return a.I == b.I && a.J == b.J;
        }

        public static bool operator !=(Position a, Position b)
        {
            return !(a == b);
        }
    }
}