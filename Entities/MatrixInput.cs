﻿using System.Collections.Generic;

namespace MatrixPath.Entities
{
    public class MatrixInput
    {
        public MatrixInput()
        {
            TraversedPaths = new HashSet<Position>();
            Positions = new Stack<Position>();
            ValidPositions = new Stack<Position>();
            BlockedCells = new HashSet<Position>();
            Grid = new int[0, 0];
        }

        public MatrixInput(int[,] grid, HashSet<Position> blockedCells)
        {
            Grid = grid;
            BlockedCells = blockedCells;
            TraversedPaths = new HashSet<Position>();
            Positions = new Stack<Position>();
            ValidPositions = new Stack<Position>();
        }

        public int[,] Grid { get; set; }

        public HashSet<Position> BlockedCells { get; set; }

        public HashSet<Position> TraversedPaths { get; set; }

        public Stack<Position> Positions { get; set; }

        public Stack<Position> ValidPositions { get; set; }
    }
}