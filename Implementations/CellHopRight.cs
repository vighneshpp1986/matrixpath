﻿using MatrixPath.Entities;
using MatrixPath.Interfaces;

namespace MatrixPath.Implementations
{
    public class CellHopRight : ICellHop
    {
        public bool TryHop(Position originalPosition, int maxRows, int maxCol, out Position nextPosition)
        {
            nextPosition = new Position(originalPosition.I, originalPosition.J);
            if (originalPosition.J < maxCol - 1)
            {
                nextPosition = new Position(originalPosition.I, originalPosition.J + 1);
                return true;
            }

            return false;
        }
    }
}