﻿using MatrixPath.Interfaces;
using Unity;

namespace MatrixPath.Implementations
{
    public static class DependencyContainer
    {
        private static readonly IUnityContainer Container;

        static DependencyContainer()
        {
            Container = new UnityContainer();
            RegisterDependencies();
        }

        private static void RegisterDependencies()
        {
            Container.RegisterType<Matrix>();
            Container.RegisterType<IGridFactory, GridFactory>();
            Container.RegisterType<IGridPrinter, GridPrinter>();
            Container.RegisterType<IBlockedCellHandler, BlockedCellHandler>();
            Container.RegisterType<IMatrixPath, MatrixPath>();
            Container.RegisterType<IConsoleWriter, ConsoleWriter>();
            Container.RegisterType<IConsoleReader<int>, ConsoleIntReader>();
            Container.RegisterType<ICellHop, CellHopRight>("CellHopRight");
            Container.RegisterType<ICellHop, CellHopDown>("CellHopDown");
            Container.RegisterType<ICellHop, CellHopDiagonal>("CellHopDiagonal");
            Container.RegisterType<IHopAction, HopAction>();
            Container.RegisterType<IBlockedCellInputAcceptanceHandler, BlockedCellInputAcceptanceHandler>();
            Container.RegisterType<IValidateAndAcceptIndividualBlockedCells, ValidateAndAcceptIndividualBlockedCells>();
        }

        public static IUnityContainer GetDependencyContainer()
        {
            return Container;
        }
    }
}