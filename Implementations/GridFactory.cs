﻿using System;
using MatrixPath.Interfaces;
using Unity;

namespace MatrixPath.Implementations
{
    public class GridFactory : IGridFactory
    {
        private readonly IConsoleReader<int> _consoleReader;

        public GridFactory(IUnityContainer container)
        {
            _consoleReader = container.Resolve<IConsoleReader<int>>();
        }

        public int[,] MakeGrid()
        {
            Console.WriteLine("Enter the size of the grid. ( Min 2 , Max 8)");
            // ReSharper disable once AssignNullToNotNullAttribute
            var size = _consoleReader.ReadLine();
            var grid = new int[size, size];
            var counter = 1;
            for (var i = 0; i < size; i++)
            for (var j = 0; j < size; j++)
            {
                grid[i, j] = counter;
                counter++;
            }

            return grid;
        }
    }
}