﻿using MatrixPath.Interfaces;
using Unity;

namespace MatrixPath.Implementations
{
    public class BlockedCellInputAcceptanceHandler : IBlockedCellInputAcceptanceHandler
    {
        private readonly IConsoleReader<int> _consoleReader;
        private readonly IConsoleWriter _consoleWriter;


        public BlockedCellInputAcceptanceHandler(IUnityContainer container)
        {
            _consoleWriter = container.Resolve<IConsoleWriter>();
            _consoleReader = container.Resolve<IConsoleReader<int>>();
        }

        public int AcceptAndValidateNumberOfCellsToBlock(int[,] grid)
        {
            int blockedCellCount;
            bool incorrect;
            do
            {
                _consoleWriter.WriteLine("Enter the number of elements to be blocked.");
                // ReSharper disable once AssignNullToNotNullAttribute
                blockedCellCount = _consoleReader.ReadLine();

                if (blockedCellCount < 0)
                {
                    _consoleWriter.WriteLine("Blocked Cell has  to be greater than or equal to 0");
                    incorrect = true;
                }
                else if (blockedCellCount > grid.Length - 2)
                {
                    _consoleWriter.WriteLine("Incorrect : First Cell and Last Cell cannot be blocked. Exiting");
                    incorrect = true;
                }
                else
                {
                    incorrect = false;
                }
            } while (incorrect);

            return blockedCellCount;
        }
    }
}