﻿using System;
using MatrixPath.Interfaces;
using Unity;

namespace MatrixPath.Implementations
{
    public class ConsoleIntReader : IConsoleReader<int>
    {
        private readonly IConsoleWriter _consoleWriter;

        public ConsoleIntReader(IUnityContainer container)
        {
            _consoleWriter = container.Resolve<IConsoleWriter>();
        }

        public int ReadLine()
        {
            bool isValid;

            int num;
            do
            {
                var value = Console.ReadLine();

                if (!int.TryParse(value, out num))
                {
                    isValid = false;
                    _consoleWriter.WriteLine("Invalid input.Expected is a number. Please try again.");
                }
                else
                {
                    isValid = true;
                }
            } while (!isValid);


            return num;
        }
    }
}