﻿using MatrixPath.Entities;
using MatrixPath.Interfaces;

namespace MatrixPath.Implementations
{
    public class CellHopDiagonal : ICellHop
    {
        public bool TryHop(Position originalPosition, int maxRows, int maxCol, out Position nextPosition)
        {
            nextPosition = new Position(originalPosition.I, originalPosition.J);
            if (originalPosition.I < maxRows - 1 && originalPosition.J < maxCol - 1)
            {
                nextPosition = new Position(originalPosition.I + 1, originalPosition.J + 1);
                return true;
            }

            return false;
        }
    }
}