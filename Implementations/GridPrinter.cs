﻿using System;
using MatrixPath.Interfaces;

namespace MatrixPath.Implementations
{
    public class GridPrinter : IGridPrinter
    {
        public void PrintGrid(int[,] grid)
        {
            for (var i = 0; i < grid.GetLength(0); i++)
            {
                for (var j = 0; j < grid.GetLength(1); j++)
                {
                    var value = grid[i, j] < 10 ? "0" + grid[i, j] : grid[i, j].ToString();
                    Console.Write($"{value} ");
                }

                Console.WriteLine(string.Empty);
            }
        }
    }
}