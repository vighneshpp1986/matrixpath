﻿using System.Collections.Generic;
using MatrixPath.Entities;
using MatrixPath.Interfaces;
using Unity;

namespace MatrixPath.Implementations
{
    public class BlockedCellHandler : IBlockedCellHandler
    {
        private readonly IBlockedCellInputAcceptanceHandler _blockedCellInputAcceptanceHandler;
        private readonly IValidateAndAcceptIndividualBlockedCells _validateAndAcceptIndividualBlockedCells;

        public BlockedCellHandler(IUnityContainer container)
        {
            _blockedCellInputAcceptanceHandler = container.Resolve<IBlockedCellInputAcceptanceHandler>();
            _validateAndAcceptIndividualBlockedCells = container.Resolve<IValidateAndAcceptIndividualBlockedCells>();
        }

        public HashSet<Position> Handle(int[,] grid)
        {
            int numberOfCellsToBlock = _blockedCellInputAcceptanceHandler.AcceptAndValidateNumberOfCellsToBlock(grid);

            var blockedHashSet =
                _validateAndAcceptIndividualBlockedCells.ValidateAndAcceptBlockedCells(grid, numberOfCellsToBlock);

            return blockedHashSet;
        }
    }
}