﻿using System;
using System.Collections.Generic;
using MatrixPath.Entities;
using MatrixPath.Interfaces;
using Unity;

namespace MatrixPath.Implementations
{
    public class ValidateAndAcceptIndividualBlockedCells : IValidateAndAcceptIndividualBlockedCells
    {

        private readonly IConsoleReader<int> _consoleReader;
        private readonly IConsoleWriter _consoleWriter;

        public ValidateAndAcceptIndividualBlockedCells(IUnityContainer container)
        {
            _consoleWriter = container.Resolve<IConsoleWriter>();
            _consoleReader = container.Resolve<IConsoleReader<int>>();
        }

        public HashSet<Position> ValidateAndAcceptBlockedCells(int[,] grid, int numberOfCellsToBlock)
        {
            var gridSize = grid.GetLength(0);
            var blockedHashSet = new HashSet<Position>();
            var maxRow = grid.GetLength(0);
            var maxCol = grid.GetLength(1);
            var startValue = grid[0, 0];
            var endValue = grid[maxRow - 1, maxCol - 1];
            var counter = 0;

            while (counter < numberOfCellsToBlock)
            {
                _consoleWriter.WriteLine("Input the cell values to be blocked.");

                // ReSharper disable once AssignNullToNotNullAttribute
                var value = _consoleReader.ReadLine();

                if (value < startValue || value > endValue)
                {
                    _consoleWriter.WriteLine(
                        $"Invalid Entry. Value should be between {startValue + 1} and {endValue - 1}.");
                }
                else if (value == startValue || value == endValue)
                {
                    _consoleWriter.WriteLine(
                        $"Invalid Entry. Value cannot be startValue {startValue} OR the end Value {endValue}");
                }
                else
                {
                    var position = GetPosition(value, gridSize);

                    if (blockedHashSet.Add(position))
                    {
                        grid[position.I, position.J] = 0;
                        counter++;
                    }
                    else
                    {
                        _consoleWriter.WriteLine("Cell has already been blocked. Please enter a new cell.");
                    }
                }
            }

            return blockedHashSet;
        }

        private Position GetPosition(int value, int gridSize)
        {
            int col;
            var rem = value % gridSize;

            if (rem == 0)
                col = gridSize;
            else
                col = rem;
            var row = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(value) / gridSize));
            row = row <= 0 ? row : row - 1;
            col = col <= 0 ? col : col - 1;
            return new Position(row, col);
        }
    }
}