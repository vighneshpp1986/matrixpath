﻿using System.Collections.Generic;
using System.Linq;
using MatrixPath.Entities;
using MatrixPath.Interfaces;
using Unity;

namespace MatrixPath.Implementations
{
    public class MatrixPath : IMatrixPath
    {
        private readonly IHopAction _hopAction;

        public MatrixPath(IUnityContainer container)
        {
            _hopAction = container.Resolve<IHopAction>();
        }

        public List<int> Compute(MatrixInput input)
        {
            var maxRow = input.Grid.GetLength(0);
            var maxCol = input.Grid.GetLength(1);

            var path = new List<int>();

            var isPathFormed = false;
            var start = new Position(0, 0);
            var end = new Position(maxRow - 1, maxCol - 1);
            //input.ValidPositions.Push(start);
            var current = start;
            do
            {
                Position next;
                var hopSuccessful = false;
                foreach (var value in _hopAction.GetHopActions())
                    if (value.TryHop(current, maxRow, maxCol, out next) && !input.BlockedCells.Contains(next) &&
                        input.TraversedPaths.Add(next))
                    {
                        input.Positions.Push(current);
                        input.ValidPositions.Push(current);
                        current = next;
                        hopSuccessful = true;
                        break;
                    }

                if (!hopSuccessful)
                {
                    if (current == start) break;
                    if (input.Positions.Any()) current = input.Positions.Pop();

                    if (input.ValidPositions.Any()) input.ValidPositions.Pop();
                }

                if (current == end)
                {
                    input.ValidPositions.Push(end);
                    isPathFormed = true;
                    break;
                }
            } while (true);

            if (isPathFormed)
            {
                path = input.ValidPositions.Select(a => input.Grid[a.I, a.J]).ToList();
                path.Reverse();
                return path;
            }

            return path;
        }
    }
}