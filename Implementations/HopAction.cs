﻿using System.Collections.Generic;
using MatrixPath.Interfaces;
using Unity;

namespace MatrixPath.Implementations
{
    public class HopAction : IHopAction
    {
        private readonly ICellHop _hopDiagonal;
        private readonly ICellHop _hopDown;
        private readonly ICellHop _hopRight;

        public HopAction(IUnityContainer container)
        {
            _hopRight = container.Resolve<ICellHop>("CellHopRight");
            _hopDown = container.Resolve<ICellHop>("CellHopDown");
            _hopDiagonal = container.Resolve<ICellHop>("CellHopDiagonal");
        }

        public List<ICellHop> GetHopActions()
        {
            // order of registration matters
            return new List<ICellHop>
            {
                // _hopDiagonal,
                _hopRight,
                _hopDown
            };
        }
    }
}