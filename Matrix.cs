﻿using System;
using System.Linq;
using MatrixPath.Entities;
using MatrixPath.Interfaces;
using Unity;

namespace MatrixPath
{
    public class Matrix
    {
        private readonly IBlockedCellHandler _blockedCellHandler;
        private readonly IConsoleWriter _consoleWriter;
        private readonly IGridFactory _gridFactory;
        private readonly IGridPrinter _gridPrinter;
        private readonly IMatrixPath _matrixPath;

        public Matrix(IUnityContainer container)
        {
            _gridFactory = container.Resolve<IGridFactory>();
            _gridPrinter = container.Resolve<IGridPrinter>();
            _blockedCellHandler = container.Resolve<IBlockedCellHandler>();
            _matrixPath = container.Resolve<IMatrixPath>();
            _consoleWriter = container.Resolve<IConsoleWriter>();
        }

        public void Start()
        {
            var input = new MatrixInput();


            input.Grid = _gridFactory.MakeGrid();

            _gridPrinter.PrintGrid(input.Grid);

            input.BlockedCells = _blockedCellHandler.Handle(input.Grid);

            _consoleWriter.WriteLine("You matrix now looks like : ");
            _gridPrinter.PrintGrid(input.Grid);

            _consoleWriter.WriteLine("");

            _consoleWriter.WriteLine("Computing path from First Cell to the last cell..");

            var path = _matrixPath.Compute(input);

            if (path.Any())
                _consoleWriter.WriteLine(string.Join("->", path));
            else
                _consoleWriter.WriteLine("No path available.");

            Console.WriteLine("");
        }
    }
}