﻿namespace MatrixPath.Interfaces
{
    public interface IGridFactory
    {
        int[,] MakeGrid();
    }
}