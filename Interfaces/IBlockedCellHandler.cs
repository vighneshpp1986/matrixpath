﻿using System.Collections.Generic;
using MatrixPath.Entities;

namespace MatrixPath.Interfaces
{
    public interface IBlockedCellHandler
    {
        HashSet<Position> Handle(int[,] grid);
    }
}