﻿namespace MatrixPath.Interfaces
{
    public interface IConsoleWriter
    {
        void WriteLine(string text);
    }
}