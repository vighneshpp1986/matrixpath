﻿using System.Collections.Generic;
using MatrixPath.Entities;

namespace MatrixPath.Interfaces
{
    public interface IMatrixPath
    {
        List<int> Compute(MatrixInput input);
    }
}