﻿using System.Collections.Generic;

namespace MatrixPath.Interfaces
{
    public interface IHopAction
    {
        List<ICellHop> GetHopActions();
    }
}