﻿using System.Collections.Generic;
using MatrixPath.Entities;

namespace MatrixPath.Interfaces
{
    public interface IValidateAndAcceptIndividualBlockedCells
    {
        HashSet<Position> ValidateAndAcceptBlockedCells(int[,] grid, int numberOfCellsToBlock);
    }
}