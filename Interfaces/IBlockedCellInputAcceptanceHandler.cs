﻿namespace MatrixPath.Interfaces
{
    public interface IBlockedCellInputAcceptanceHandler
    {
        int AcceptAndValidateNumberOfCellsToBlock(int[,] grid);
    }
}