﻿namespace MatrixPath.Interfaces
{
    public interface IGridPrinter
    {
        void PrintGrid(int[,] grid);
    }
}