﻿using MatrixPath.Entities;

namespace MatrixPath.Interfaces
{
    public interface ICellHop
    {
        bool TryHop(Position originalPosition, int maxRows, int maxCol, out Position nextPosition);
    }
}