﻿namespace MatrixPath.Interfaces
{
    public interface IConsoleReader<T>
    {
        T ReadLine();
    }
}