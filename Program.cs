﻿using System;
using MatrixPath.Implementations;
using Unity;

namespace MatrixPath
{
    internal class Program
    {
        private static void Main()
        {
            var container = DependencyContainer.GetDependencyContainer();
            var matrix = container.Resolve<Matrix>();
            matrix.Start();
            Console.WriteLine("Press any key to exit");
            Console.ReadLine();
        }
    }
}